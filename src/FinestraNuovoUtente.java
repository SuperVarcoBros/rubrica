
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraNuovoUtente extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	protected ArrayList<Persona> contatti = new ArrayList<>();
	protected FinestraPrincipale finestra_principale;
	
	private static String titolo = "Editor Persona";
	private int larghezza=500, altezza=300;
	private JPanel pannello_centrale = new JPanel(new GridLayout(5, 2)),
			pannello_sud = new JPanel();

	private JLabel etichetta_nome = new JLabel("Nome:"), 
		etichetta_cognome = new JLabel("Cognome:"), 
		etichetta_indirizzo = new JLabel("Indirizzo:"), 
		etichetta_telefono = new JLabel("Telefono:"), 
		etichetta_eta = new JLabel("Età:");
	
	protected JTextField campo_nome = new JTextField(),
			campo_cognome = new JTextField(),
			campo_indirizzo = new JTextField(),
			campo_telefono = new JTextField(),
			campo_eta = new JTextField();
	
	private JButton bottone_conferma = new JButton("Conferma"),
			bottone_annulla = new JButton("Annulla");
	
	public FinestraNuovoUtente(ArrayList<Persona> contatti, FinestraPrincipale finestra_principale) {
		super(titolo);
		this.setSize(larghezza, altezza);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Container frmContentPane = this.getContentPane();
		frmContentPane.add(pannello_centrale, BorderLayout.CENTER);
		frmContentPane.add(pannello_sud, BorderLayout.SOUTH);
		
		this.contatti = contatti;
		this.finestra_principale = finestra_principale;
		
		//TODO: devo capire comme andare a capo dopo ogni componente
		pannello_centrale.add(etichetta_nome);
		campo_nome.setPreferredSize(new Dimension(150, 20));
		pannello_centrale.add(campo_nome);
		
		pannello_centrale.add(etichetta_cognome);
		campo_cognome.setPreferredSize(new Dimension(150, 20));
		pannello_centrale.add(campo_cognome);

		
		pannello_centrale.add(etichetta_indirizzo);
		campo_indirizzo.setPreferredSize(new Dimension(150, 20));
		pannello_centrale.add(campo_indirizzo);
		
		pannello_centrale.add(etichetta_telefono);
		campo_telefono.setPreferredSize(new Dimension(150, 20));
		pannello_centrale.add(campo_telefono);
		
		pannello_centrale.add(etichetta_eta);
		campo_eta.setPreferredSize(new Dimension(150, 20));
		pannello_centrale.add(campo_eta);
		
		pannello_sud.add(bottone_conferma);
		bottone_conferma.addActionListener(this);
		pannello_sud.add(bottone_annulla);
		bottone_annulla.addActionListener(this);
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String testo;
		int eta = -1;
		JButton b = (JButton) e.getSource();
		switch(b.getText()) {
		case "Conferma":
			if(campo_nome.getText().isEmpty() || campo_cognome.getText().isEmpty()
					|| campo_indirizzo.getText().isEmpty() || campo_telefono.getText().isEmpty()
					|| campo_eta.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null,"Prima di confermare inserire tutti i campi");
			}
			else {
				try {
					testo = campo_eta.getText().trim();
					eta = Integer.parseInt(testo);
					contatti.add(new Persona(campo_nome.getText(), campo_cognome.getText(), 
							campo_indirizzo.getText(), campo_telefono.getText(), eta));
					PersistenzaDati.aggiorna_contatti(contatti);
					this.dispose();
					finestra_principale.dispose();
					new FinestraPrincipale(contatti);
				}
				catch(java.lang.NumberFormatException no) {
					JOptionPane.showMessageDialog(null,"Età deve essere un numero");
				}
			}
			break;
		case "Annulla":
			this.dispose();
			break;
		}
	}

}
