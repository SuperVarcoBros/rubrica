import java.util.ArrayList;

public class Utente {
	
	private String nome, cognome, username, password;
	
	public Utente(String nome, String cognome, String username, String password) {
		this.nome = nome;
		this.cognome = cognome;
		this.username = username;
		this.password = password;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	static boolean ceck_utente(ArrayList<Utente> utenti, String user, String pass) {
		for (Utente utente : utenti) {
			if(utente.getUsername().equals(user) && utente.getPassword().equals(pass))
				return true;
		}
		return false;
	}

}
