import java.util.ArrayList;

public class Main {
	
	static Rubrica rubrica = new Rubrica();
	static ArrayList<Utente> utenti = new ArrayList<>();
	
	public static void main(String[] args) {

		PersistenzaDati.crea_file("informazioni.txt");
		PersistenzaDati.crea_file("utenti.txt");
		//FinestraPrincipale myFrm = new FinestraPrincipale(PersistenzaDati.carica_contatti());
		new FinestraLogin(PersistenzaDati.carica_utenti());
	}

}
