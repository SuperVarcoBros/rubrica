
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class FinestraModifica extends FinestraNuovoUtente {
	
	private static final long serialVersionUID = 1L;
	private Persona utente_selezionato;
	private int id_utente_selezionato;

	public FinestraModifica(ArrayList<Persona> contatti, FinestraPrincipale finestra_principale, int id_utente_selezionato) {
		super(contatti, finestra_principale);
		this.id_utente_selezionato = id_utente_selezionato;
		utente_selezionato = contatti.get(id_utente_selezionato);
		int eta = utente_selezionato.getEta();
		campo_nome.setText(utente_selezionato.getNome());
		campo_cognome.setText(utente_selezionato.getCognome());
		campo_indirizzo.setText(utente_selezionato.getIndirizzo());
		campo_telefono.setText(utente_selezionato.getTelefono());
		campo_eta.setText(Integer.toString(eta));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String testo;
		int eta = -1;
		JButton b = (JButton) e.getSource();
		switch(b.getText()) {
		case "Conferma":
			if(campo_nome.getText().isEmpty() || campo_cognome.getText().isEmpty()
					|| campo_indirizzo.getText().isEmpty() || campo_telefono.getText().isEmpty()
					|| campo_eta.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null,"Prima di confermare inserire tutti i campi");
			}
			else {
				try{
					testo = campo_eta.getText().trim();
					eta = Integer.parseInt(testo);
					System.out.println();
					contatti.get(id_utente_selezionato).setAll(campo_nome.getText(), campo_cognome.getText(), 
							campo_indirizzo.getText(), campo_telefono.getText(), eta);
					PersistenzaDati.aggiorna_contatti(contatti);
					this.dispose();
					finestra_principale.dispose();
					new FinestraPrincipale(contatti);
				}
				catch(java.lang.NumberFormatException no) {
					JOptionPane.showMessageDialog(null,"Età deve essere un numero");
				}
			}
			break;
		case "Annulla":
			this.dispose();
			break;
		}
	}

}
