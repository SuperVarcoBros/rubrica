import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersistenzaDati {
	
	public static void crea_file(String nomefile) {
		String nome_os = System.getProperty("os.name", "").toLowerCase();
		if(nome_os.startsWith("windows")) {
			try {
				File file = new File(nomefile);
				if(!file.exists())
					file.createNewFile();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		else if(nome_os.startsWith("linux")) {
			try {
				File file = new File(nomefile);
				if(!file.exists())
					file.createNewFile();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static ArrayList<Persona> carica_contatti() {
		ArrayList<Persona> contatti = new ArrayList<>();
		Pattern p;
		Matcher m;
		try {
			FileInputStream fstream = new FileInputStream("informazioni.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strline;
			while((strline = br.readLine()) != null) {
				p = Pattern.compile("(.+);(.+);(.+);(.+);(.+)");
				m = p.matcher(strline);
				if(m.find()) {
					int eta = Integer.parseInt(m.group(5));
					Persona persona = new Persona(m.group(1), m.group(2), m.group(3), m.group(4), eta);
					contatti.add(persona);
				}
			}
			br.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		return contatti;
	}
	
	public static void aggiorna_contatti(ArrayList<Persona> contatti) {
		
		try {
			FileWriter fw = new FileWriter("informazioni.txt");
			String linea =  "";
			for (Persona persona : contatti) {
				linea += persona.getNome()+";"+persona.getCognome()+";"+persona.getIndirizzo()+";"+persona.getTelefono()+";"+persona.getEta()+"\n";
			}
			fw.write(linea);
			fw.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Utente> carica_utenti() {
		ArrayList<Utente> utenti = new ArrayList<>();
		Pattern p;
		Matcher m;
		try {
			FileInputStream fstream = new FileInputStream("utenti.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strline;
			while((strline = br.readLine()) != null) {
				p = Pattern.compile("(.+);(.+);(.+);(.+)");
				m = p.matcher(strline);
				if(m.find()) {
					Utente utente = new Utente(m.group(1), m.group(2), m.group(3), m.group(4));
					utenti.add(utente);
				}
			}
			br.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		return utenti;
	}
	
public static void aggiorna_utenti(ArrayList<Utente> utenti) {
		
		try {
			FileWriter fw = new FileWriter("utenti.txt");
			String linea =  "";
			for (Utente utente : utenti) {
				linea += utente.getNome()+";"+utente.getCognome()+";"+utente.getUsername()+";"+utente.getPassword()+"\n";
			}
			fw.write(linea);
			fw.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
