
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class FinestraPrincipale extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private ArrayList<Persona> contatti = new ArrayList<>();
	
	private static String titolo = "Rubrica";
	private int larghezza=500, altezza=300;
	
	//Pannello Centrale:
	private JPanel pannello_centrale = new JPanel();
	private JTable jTable;
	//private JTable tabella = new JTable();
	
	//Pannello Sud:
	private JButton pulsante_nuovo = new JButton("Nuovo");
	private JButton pulsante_modifica = new JButton("Modifica");
	private JButton pulsante_elimina = new JButton("Elimina");
	private JPanel pannello_sud = new JPanel();
	
	public FinestraPrincipale(ArrayList<Persona> contatti) {
		super(titolo);
		this.setSize(larghezza, altezza);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.contatti = contatti;
		
		//Pannello Centro
		String[] nomi_colonne = {"Nome", "Cognome", "Telefono"};
		
		String informazioni[][] = new String[contatti.size()][nomi_colonne.length];
		for(int i=0; i<contatti.size(); i++) {
			informazioni[i][0] = contatti.get(i).getNome();
			informazioni[i][1] = contatti.get(i).getCognome();
			informazioni[i][2] = contatti.get(i).getTelefono();
		}

		DefaultTableModel model = new DefaultTableModel(informazioni, nomi_colonne);
		jTable = new JTable(model);
		pannello_centrale.add(new JScrollPane(jTable));

		//Pannello Sud
		pannello_sud.add(pulsante_nuovo);
		pannello_sud.add(pulsante_modifica);
		pannello_sud.add(pulsante_elimina);
		//Container Principale
		Container frmContentPane = this.getContentPane();
		frmContentPane.add(pannello_centrale, BorderLayout.CENTER);
		frmContentPane.add(pannello_sud, BorderLayout.SOUTH);
		//Impostiamo le proprieta� di visualizzazione
		//this.pack(); // Imposta la dimensione minima per visualizzare tutti i componenti
		// Posizioniamo la finestra al centro dello schermo
		this.setLocationRelativeTo(null);
		pulsante_nuovo.addActionListener(this);
		pulsante_modifica.addActionListener(this);
		pulsante_elimina.addActionListener(this);
		// Rendiamo visibile la finestra
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		switch(b.getText()) {
		case "Nuovo":
			new FinestraNuovoUtente(contatti, FinestraPrincipale.this);
			break;
		case "Modifica":
			if(jTable.getSelectedRow() == -1)
				JOptionPane.showMessageDialog(null,"Selezionare un utente della lista");
			else
				new FinestraModifica(contatti, FinestraPrincipale.this, jTable.getSelectedRow());
			break;
		case "Elimina":
			if(jTable.getSelectedRow() == -1)
				JOptionPane.showMessageDialog(null,"Selezionare un utente della lista");
			else {
				int conferma = JOptionPane.showConfirmDialog(null, "Eliminare "+contatti.get(jTable.getSelectedRow()).getNome()+" "+contatti.get(jTable.getSelectedRow()).getCognome()+"?", "Conferma eliminazione", JOptionPane.YES_NO_OPTION);
				if(conferma == 0) {
					contatti.remove(jTable.getSelectedRow());
					PersistenzaDati.aggiorna_contatti(contatti);
					this.dispose();
					new FinestraPrincipale(contatti);
				}
			}
			break;
		}
	}


}
