import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraLogin extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Utente> utenti = new ArrayList<Utente>();
	
	private static String titolo = "Login";
	private int larghezza=230, altezza=120;
	private JPanel pannello_centrale = new JPanel(new GridLayout(2, 2)),
			pannello_sud = new JPanel();

	private JLabel etichetta_username = new JLabel("Username:"), 
		etichetta_password = new JLabel("Password:");
	
	protected JTextField campo_username = new JTextField(),
			campo_password = new JTextField();
	
	private JButton bottone_conferma = new JButton("Conferma"),
			bottone_registrati = new JButton("Registrati"),
			bottone_annulla = new JButton("Annulla");
	
	public FinestraLogin(ArrayList<Utente> utenti) {
		super(titolo);
		this.utenti = PersistenzaDati.carica_utenti();
		this.setSize(larghezza, altezza);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container frmContentPane = this.getContentPane();
		frmContentPane.add(pannello_centrale, BorderLayout.CENTER);
		frmContentPane.add(pannello_sud, BorderLayout.SOUTH);
		
		this.utenti = utenti;
				
		pannello_centrale.add(etichetta_username);
		campo_username.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_username);
		
		pannello_centrale.add(etichetta_password);
		campo_username.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_password);
		
		pannello_sud.add(bottone_conferma);
		bottone_conferma.addActionListener(this);
		pannello_sud.add(bottone_registrati);
		bottone_registrati.addActionListener(this);
		pannello_sud.add(bottone_annulla);
		bottone_annulla.addActionListener(this);
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		switch(b.getText()) {
		case "Conferma": {
			if(Utente.ceck_utente(utenti, campo_username.getText(), campo_password.getText())) {
				this.dispose();
				new FinestraPrincipale(PersistenzaDati.carica_contatti());
			} else JOptionPane.showMessageDialog(null,"Utente o password errati");
				break;
		}
		case "Registrati": {
			new FinestraRegistrazione(this); break;
		}
		case "Annulla": 
			this.dispose(); 
			break;
		}
	}

}
