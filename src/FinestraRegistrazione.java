import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraRegistrazione extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Utente> utenti = new ArrayList<>();
	private FinestraLogin finestralogin;

	private static String titolo = "Registrazione";
	private int larghezza=380, altezza=250;
	
	private JPanel pannello_centrale = new JPanel(new GridLayout(4, 2)),
			pannello_sud = new JPanel();

	private JLabel etichetta_username = new JLabel("Username:"), 
		etichetta_password = new JLabel("Password:"),
		etichetta_nome = new JLabel("Nome:"),
		etichetta_cognome = new JLabel("Cognome:");
	
	protected JTextField campo_username = new JTextField(),
			campo_password = new JTextField(),
			campo_nome = new JTextField(),
			campo_cognome = new JTextField();
	
	private JButton bottone_conferma = new JButton("Conferma"),
			bottone_annulla = new JButton("Annulla");
	
	public FinestraRegistrazione(FinestraLogin finestralogin) {
		super(titolo);
		this.utenti = PersistenzaDati.carica_utenti();
		this.finestralogin = finestralogin;
		this.setSize(larghezza, altezza);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Container frmContentPane = this.getContentPane();
		frmContentPane.add(pannello_centrale, BorderLayout.CENTER);
		frmContentPane.add(pannello_sud, BorderLayout.SOUTH);
		
		pannello_centrale.add(etichetta_nome);
		campo_nome.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_nome);
		
		pannello_centrale.add(etichetta_cognome);
		etichetta_cognome.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_cognome);
		
		pannello_centrale.add(etichetta_username);
		campo_username.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_username);
		
		pannello_centrale.add(etichetta_password);
		campo_username.setPreferredSize(new Dimension(15, 20));
		pannello_centrale.add(campo_password);
		
		pannello_sud.add(bottone_conferma);
		bottone_conferma.addActionListener(this);
		pannello_sud.add(bottone_annulla);
		bottone_annulla.addActionListener(this);
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		switch(b.getText()) {
		case "Annulla": this.dispose();break;
		case "Conferma": {
			utenti.add(new Utente(campo_nome.getText(), campo_cognome.getText(), campo_username.getText(), campo_password.getText()));
			PersistenzaDati.aggiorna_utenti(utenti);
			JOptionPane.showMessageDialog(null,"Registrazione eseguita");
			finestralogin.dispose();
			this.dispose();
			new FinestraLogin(utenti);
			break;
		}
		}
	}
	
	

}
