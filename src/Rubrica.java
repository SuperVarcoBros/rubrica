import java.util.ArrayList;

public class Rubrica {

	private ArrayList<Persona> contatti = new ArrayList<>();

	public ArrayList<Persona> getContatti() {
		return contatti;
	}

	public void setContatti(ArrayList<Persona> contatti) {
		this.contatti = contatti;
	}

}
